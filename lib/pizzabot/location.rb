# frozen_string_literal: true

module PizzaBot
  class Location
    include Comparable

    attr_accessor :x, :y

    def initialize(params)
      @x, @y = params
    end

    def <=>(other)
      # Comparison between two location
      # 
      # Comparison is measured by the distance from the origin [0,0]
      # Rule is: attribute @x has precedence over attribute @y
      # Meaning [2,2] is lower then [3,1], because [2,2] is closer to [0,0]
      if x == other.x
        y <=> other.y
      else
        x <=> other.x
      end
    end

    def hash
      [x, y].hash
    end

    def to_s
      [x, y].join(',')
    end
    alias inspect to_s

    def direction_to(other)
      # Locations need to be sorted before finding direction.
      # 
      # Always start with the primary (@x) coordinate (going East),
      # Then finding the path by @y coordinate
      go_east = 'E' * (other.x - x)

      north_or_south = other.y - y
      sigh = north_or_south > -1 ? 'N' : 'S'

      go_east + (sigh * north_or_south.abs)
    end

    def valid?
      # params x and y must be present in the param string
      # example of invalid param string: '(4,)' or '(4,5'
      !x.nil? && !y.nil?
    end

    class << self
      def most_south_westerly(locations)
        min_xy = (minmax_by_x(locations) & minmax_by_y(locations)).min

        PizzaBot::Location.new([min_xy, min_xy])
      end

      def most_north_easterly(locations)
        max_xy = (minmax_by_x(locations) & minmax_by_y(locations)).max

        PizzaBot::Location.new([max_xy, max_xy])
      end

      def minmax_by_x(locations)
        locations
          .minmax_by(&:x)
          .flat_map { |location| [location.x, location.y] }
      end

      def minmax_by_y(locations)
        locations
          .minmax_by(&:y)
          .flat_map { |location| [location.x, location.y] }
      end

      def valid?(locations)
        locations.all?(&:valid?)
      end
    end
  end
end
