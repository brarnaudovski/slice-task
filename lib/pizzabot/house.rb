# frozen_string_literal: true

module PizzaBot
  class House
    START = '0,0'
    REGEX = /-*\d+/.freeze

    include Comparable

    attr_reader :location

    def initialize(params)
      @location = PizzaBot::Location.new(params.first.scan(REGEX).map(&:to_i))
    end

    def <=>(other)
      location <=> other.location
    end

    def self.start
      new(['0,0'])
    end

    def valid?
      location.valid?
    end

    def to_s
      "House@location: #{location}"
    end

    def direction_to(other)
      location.direction_to other.location
    end
  end
end
