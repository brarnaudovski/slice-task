# frozen_string_literal: true

module PizzaBot
  class Grid
    REGEX = /-*\d+/.freeze

    WEST_POINT = 0
    SOUTH_POINT = 0

    attr_reader :west, :east
    attr_reader :south, :north

    def initialize(param)
      return if param.empty?

      @west = WEST_POINT
      @south = SOUTH_POINT

      @east, @north = param.first.scan(REGEX).map(&:to_i)
    end

    def most_south_westerly_location
      PizzaBot::Location.new([west, south])
    end

    def most_north_easterly_location
      PizzaBot::Location.new([east, north])
    end

    def valid?
      !empty? && !negative?
    end

    private

    def empty?
      east.nil? || north.nil?
    end

    def negative?
      east.negative? || north.negative?
    end
  end
end
