# frozen_string_literal: true

module PizzaBot
  class Validator
    class InvalidGrid < StandardError; end
    class HouseLocationOutOfGrid < StandardError; end

    attr_reader :input

    def initialize(input)
      @input = input
    end

    def validate!
      raise(InvalidGrid) unless valid_grid?
      raise(HouseLocationOutOfGrid) if any_location_out_of_grid?
    end

    def valid?
      valid_grid? && !any_location_out_of_grid?
    end

    private

    def valid_grid?
      input.grid.valid?
    end

    def any_location_out_of_grid?
      return false if input.houses.empty?

      compare_by_most_south_westerly || compare_by_most_north_easterly
    end

    def compare_by_most_south_westerly
      house_location_at_most_south_westerly <
        grid_location_at_most_south_westerly
    end

    def compare_by_most_north_easterly
      house_location_at_most_north_easterly >
        grid_location_at_most_north_easterly
    end

    def grid_location_at_most_south_westerly
      input.grid.most_south_westerly_location
    end

    def grid_location_at_most_north_easterly
      input.grid.most_north_easterly_location
    end

    def house_location_at_most_south_westerly
      PizzaBot::Location.most_south_westerly(input.houses.map(&:location))
    end

    def house_location_at_most_north_easterly
      PizzaBot::Location.most_north_easterly(input.houses.map(&:location))
    end
  end
end
