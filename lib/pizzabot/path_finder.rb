# frozen_string_literal: true

module PizzaBot
  class PathFinder
    EMPTY_PATH = ''

    def initialize(input)
      @houses = input.houses
    end

    def print
      return EMPTY_PATH if @houses.empty?

      paths = []
      make_pairs.each_slice(2) do |pair|
        paths << find_between(pair[0], pair[1])
      end

      paths.map { |path| path << 'D' }.join
    end

    private

    def make_pairs
      start = PizzaBot::House.start

      [start] +
        @houses[0..-2].flat_map { |house| [house, house] } +
        [@houses.last]
    end

    def find_between(from_house, to_house)
      from_house.direction_to to_house
    end
  end
end
