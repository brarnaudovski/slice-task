# frozen_string_literal: true

module PizzaBot
  class Input
    HOUSES_INPUT_REGEX = /(\(-*\d*,\s*-*\d*)\)/.freeze
    GRID_INPUT_REGEX = /(^-*\d+x-*\d+)/.freeze

    attr_reader :grid, :houses

    def initialize(command)
      grid_params = command.scan(GRID_INPUT_REGEX).flatten
      houses = command.scan(HOUSES_INPUT_REGEX)

      @grid = Grid.new(grid_params)
      @houses = DeliveryHouse.bulk_create(houses).sort
    end

    def instruction
      validator.validate!
      pathfinder.print
    end

    private

    def validator
      @validator ||= PizzaBot::Validator.new(self)
    end

    def pathfinder
      @pathfinder ||= PizzaBot::PathFinder.new(self)
    end
  end
end
