# frozen_string_literal: true

module PizzaBot
  class DeliveryHouse
    class << self
      def bulk_create(houses)
        # Houses with invalid locations are not saved
        houses.reduce([]) do |memo, house| 
          build_house = PizzaBot::House.new(house)

          memo << build_house if build_house.valid?

          memo
        end
      end
    end
  end
end
