# frozen_string_literal: true

RSpec.describe PizzaBot::DeliveryHouse do
  let(:input) { [['0,0'], ['1, 3'], ['1, 0']] }

  subject(:houses) { PizzaBot::DeliveryHouse.bulk_create(input) }

  it 'is able to create houses in large quantities' do
    expect(houses).to include PizzaBot::House.new(input.first)
  end

  it 'can sort the houses' do
    houses_sort = houses.sort.map(&:location).map(&:inspect)
    expected = ['0,0', '1,0', '1,3']

    expect(houses_sort).to eq expected
  end
end
