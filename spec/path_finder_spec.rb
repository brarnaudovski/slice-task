# frozen_string_literal: true

RSpec.describe PizzaBot::PathFinder do
  let(:input_string) { '5x5 (1,3) (4,4), (0,1)' }
  let(:input) { PizzaBot::Input.new(input_string) }

  subject { PizzaBot::PathFinder.new(input) }

  it 'prints the instruction' do
    expect(subject.print).to eq 'NDENNDEEEND'
  end

  context 'without delivery' do
    input = PizzaBot::Input.new('5x5')

    subject(:without_delivery) { PizzaBot::PathFinder.new(input) }

    it 'prints empty instruction' do
      expect(without_delivery.print).to eq PizzaBot::PathFinder::EMPTY_PATH
    end
  end
end
