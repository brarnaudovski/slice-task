# frozen_string_literal: true

RSpec.describe PizzaBot::House do
  let(:params) { ['0,5'] }
  let(:location) { PizzaBot::Location.new([0, 5]) }

  subject { PizzaBot::House.new(params) }

  it 'has valid location' do
    expect(subject.location).to eq location
  end

  context 'comparison' do
    it { is_expected.to be > PizzaBot::House.start }
  end
end
