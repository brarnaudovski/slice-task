# frozen_string_literal: true

RSpec.describe PizzaBot do
  it 'has a version number' do
    expect(PizzaBot::VERSION).not_to be nil
  end
end
