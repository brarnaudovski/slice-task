# frozen_string_literal: true

RSpec.describe PizzaBot::Validator do
  def build_validator(params)
    PizzaBot::Input
      .new(params)
      .then { |input| PizzaBot::Validator.new(input) }
  end

  context 'valid house locations' do
    it 'existing list of locations' do
      validator = build_validator('5x5 (1,3) (4,4), (0,1)')

      expect(validator.valid?).to be true
    end

    it 'empty list of locations' do
      validator = build_validator('5x5 ')

      expect(validator.valid?).to be true
    end
  end

  context 'invalid house locations' do
    it 'contains location greater than the Grid upper boundary' do
      validator = build_validator('5x5 (1,3) (4,4), (0,6)')

      expect(validator.valid?).to be false
    end

    it 'contains location smaller than the Grid Start position' do
      validator = build_validator('5x5 (1,3) (-4,4), (0,2)')

      expect(validator.valid?).to be false
    end

    it 'raises an error' do
      validator = build_validator('5x5 (1,3) (-4,4), (0,2)')

      expect { validator.validate! }.to raise_error(PizzaBot::Validator::HouseLocationOutOfGrid)
    end
  end

  context 'valid grid' do
    it 'existing grid object' do
      validator = build_validator('5x5 (1,3) (4,4), (0,2)')

      expect(validator.valid?).to be true
    end
  end

  context 'invalid grid' do
    it 'missing grid parameters' do
      validator = build_validator('(1,3) (4,4), (0,6)')

      expect(validator.valid?).to be false
    end

    it 'contains negative parameters' do
      validator = build_validator('5x-5 (1,3) (4,4), (0,6)')

      expect(validator.valid?).to be false
    end

    it 'raises an error' do
      validator = build_validator('5x-5 (1,3) (4,4), (0,2)')

      expect { validator.validate! }.to raise_error(PizzaBot::Validator::InvalidGrid)
    end
  end
end
