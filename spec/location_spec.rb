# frozen_string_literal: true

RSpec.describe PizzaBot::Location do
  let(:params) { [2, 3] }

  subject(:location) { PizzaBot::Location.new(params) }

  def compare_location(action)
    case action
    when :addition
      PizzaBot::Location.new(params.dup.map { |e| e + 1 })
    when :substitution
      PizzaBot::Location.new(params.dup.map { |e| e - 1 })
    when :same_by_x
      PizzaBot::Location.new([params.first, params.last + 1])
    when :eql
      subject
    end
  end

  context 'initialize object' do
    it { is_expected.to have_attributes(x: 2, y: 3) }
  end

  context 'comparison' do
    it 'with location which is more distant to Start location' do
      expect(subject).to be < compare_location(:addition)
    end

    it 'with location which is closer to Start location' do
      expect(subject).to be > compare_location(:substitution)
    end

    it 'by primary coordinate' do
      expect(subject).to be < compare_location(:same_by_x)
    end

    it 'is equal' do
      expect(subject).to eq compare_location(:eql)
    end
  end
end
