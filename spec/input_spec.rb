# frozen_string_literal: true

RSpec.describe PizzaBot::Input do
  let(:input_string) { '5x5 (1,3) (4,4), (0,1)' }

  subject(:input) { PizzaBot::Input.new(input_string) }

  context 'grid object' do
    it 'initiate the object' do
      expect(input.grid).to be_instance_of PizzaBot::Grid
    end

    it 'has valid attributes' do
      grid_attr = { west: 0, east: 5, south: 0, north: 5 }

      expect(input.grid).to have_attributes grid_attr
    end
  end

  context 'list of houses' do
    it 'initiate the list' do
      expect(input.houses).to be_instance_of Array
    end

    it 'include House objects' do
      expect(input.houses).to include PizzaBot::House.new([input_string[-5, 5]])
    end
  end

  context 'delivery path' do
    it 'prints instructions' do
      expect(input.instruction).to eq 'NDENNDEEEND'
    end
  end
end
