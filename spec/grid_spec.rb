# frozen_string_literal: true

RSpec.describe PizzaBot::Grid do
  let(:params) { ['5x5'] }

  subject { PizzaBot::Grid.new(params) }

  it 'has valid attributes' do
    expected_attr = {
      west: 0,
      south: 0,
      east: 5,
      north: 5
    }

    expect(subject).to have_attributes expected_attr
  end
end
