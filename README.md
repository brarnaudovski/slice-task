# PizzaBot

Slice Tech Test

The main logic of the solution is to find the path between two location, which are pre-sorted.
Meaning, to find the correct path, the list of delivery houses need to be sorted.

## Installation
Make sure you have [`bundler`](https://bundler.io/) installed on your system.
Then, you can run:

    $ bundle install

Or run from script:

    $ ./bin/setup

## Usage

From your terminal, run `pizzabot` script located in the root of the project. You need to pass command to the script. For example:

    $ ./pizzabot '5x5 (0, 0) (4, 2) (0, 1)'

## Development

After checking out the repo, run `bin/setup` to install dependencies. Then, run `rake spec` to run the tests. You can also run `bin/console` for an interactive prompt that will allow you to experiment.
